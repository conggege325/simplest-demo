FROM registry.cn-hangzhou.aliyuncs.com/conggege325/pub-repository:8-jdk-alpine-timezone-font
ADD target/simplest-demo-1.0.0.jar /app.jar
ENTRYPOINT ["java","-jar","app.jar","--spring.profiles.active=prod"]
