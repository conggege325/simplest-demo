package com.example.demo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.generator.FastAutoGenerator;
import com.baomidou.mybatisplus.generator.config.DataSourceConfig;
import com.baomidou.mybatisplus.generator.config.OutputFile;
import com.baomidou.mybatisplus.generator.config.converts.MySqlTypeConvert;
import com.baomidou.mybatisplus.generator.config.po.TableInfo;
import com.baomidou.mybatisplus.generator.config.rules.DbColumnType;
import com.baomidou.mybatisplus.generator.engine.FreemarkerTemplateEngine;

import java.io.File;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class CodeGen {

    public static void main(String[] args) {

        String projectPath = "D:\\dev\\workspaces\\simplest-demo";

//        DataSourceConfig.Builder dataSourceConfigBuilder = new DataSourceConfig.Builder("jdbc:mysql://localhost:3306/test?useUnicode=true&characterEncoding=utf8&useSSL=false&serverTimezone=GMT", "root", "123456");
        DataSourceConfig.Builder dataSourceConfigBuilder = new DataSourceConfig.Builder("jdbc:postgresql://localhost:5432/test?serverTimezone=UTC&useUnicode=true&characterEncoding=utf8&useSSL=false", "root", "123456");
        dataSourceConfigBuilder.typeConvert((globalConfig, fieldType) -> {
            String t = fieldType.toLowerCase();
            if (t.equals("datetime") || t.equals("date")) {
                return DbColumnType.DATE;
            }
            if (t.equals("tinyint")) {
                return DbColumnType.INTEGER;
            }
            //其它字段采用默认转换（非mysql数据库可以使用其它默认的数据库转换器）
            return new MySqlTypeConvert().processTypeConvert(globalConfig, fieldType);
        });

        FastAutoGenerator.create(dataSourceConfigBuilder)
                //默认使用Velocity，但是Freemarker扩展起来比Velocity方便
                .templateEngine(new FreemarkerTemplateEngine() {
                    @Override
                    protected void outputCustomFile(Map<String, String> customFile, TableInfo tableInfo, Map<String, Object> objectMap) {
                        String entityName = tableInfo.getEntityName();
                        String entityPath = this.getPathInfo(OutputFile.entity);
                        File genFile = new File(entityPath).getParentFile();
                        this.outputFile(new File(genFile, "dto" + File.separator + entityName + "DTO.java"), objectMap, customFile.get("DTO.java"));
                        this.outputFile(new File(genFile, "form" + File.separator + entityName + "Form.java"), objectMap, customFile.get("Form.java"));
                    }
                })
                // 全局配置
                .globalConfig((scanner, builder) -> {
                    builder.author("CGG")
                            .fileOverride()
                            .enableSwagger()
                            .outputDir(projectPath + "\\src\\main\\java")
                            .disableOpenDir();
                })
                // 包配置
                .packageConfig((scanner, builder) -> {
                    builder.parent("org.example.demo")
                            .entity("entity.po")
                            .pathInfo(Collections.singletonMap(OutputFile.mapperXml, projectPath + "\\src\\main\\resources\\mapper"));
                })
                // 策略配置
                .strategyConfig((scanner, builder) -> {
                    builder.addInclude("bd_vehicle")
                            .addTablePrefix("ac_", "t_jc_", "bd_")
                            .addFieldPrefix("c_", "n_", "d_");
                    //controller的策略配置
                    builder.controllerBuilder()
                            .enableRestStyle()
                            .build();
                    //service的策略配置
                    builder.serviceBuilder()
                            .formatServiceFileName("%sService")
                            .formatServiceImplFileName("%sServiceImpl")
                            .build();
                    //entity的策略配置
                    builder.entityBuilder()
                            .enableLombok()
                            .enableTableFieldAnnotation()
                            .idType(IdType.ASSIGN_ID)
                            .build();
                    //mapper的策略配置
                    builder.mapperBuilder()
                            .enableMapperAnnotation()
                            .enableBaseColumnList()
                            .enableBaseResultMap()
                            .build();
                })
                .templateConfig((scanner, builder) -> {
                    builder
                            .entity("/templates/entity.java") //注意，此处不要加.ftl后缀
                            .build();
                })
                // 自定义模板
                .injectionConfig(consumer -> {
                    Map<String, String> customFile = new HashMap<>();
                    customFile.put("DTO.java", "/templates/entityDTO.java.ftl"); // DTO
                    customFile.put("Form.java", "/templates/entityForm.java.ftl"); // form
                    consumer.customFile(customFile);
                })
                .execute();
    }
}
