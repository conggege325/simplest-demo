package ${package.Parent}.entity.dto;

import ${package.Entity}.${entity};
<#if swagger>
import io.swagger.annotations.ApiModel;
</#if>
<#if entityLombokModel>
import lombok.Getter;
import lombok.Setter;
    <#if chainModel>
import lombok.experimental.Accessors;
    </#if>
</#if>
<#if entitySerialVersionUID>
import java.io.Serializable;
</#if>

/**
 * <p>${table.comment!}</p>
 *
 * @author ${author}
 * @since ${date}
 */
<#if entityLombokModel>
@Getter
@Setter
    <#if chainModel>
@Accessors(chain = true)
    </#if>
</#if>
<#if swagger>
@ApiModel(value = "${entity}-DTO对象", description = "${table.comment!}")
</#if>
<#if entitySerialVersionUID>
public class ${entity}DTO extends ${entity} implements Serializable {
<#else>
public class ${entity}DTO extends ${entity} {
</#if>
<#if entitySerialVersionUID>

    private static final long serialVersionUID = 1L;
</#if>
}
