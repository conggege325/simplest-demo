package ${package.Parent}.entity.form;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
<#if swagger>
import io.swagger.annotations.ApiModel;
</#if>
<#if entityLombokModel>
import lombok.Getter;
import lombok.Setter;
    <#if chainModel>
import lombok.experimental.Accessors;
    </#if>
</#if>
<#if entitySerialVersionUID>
import java.io.Serializable;
</#if>

/**
 * <p>${table.comment!}</p>
 *
 * @author ${author}
 * @since ${date}
 */
<#if entityLombokModel>
@Getter
@Setter
    <#if chainModel>
@Accessors(chain = true)
    </#if>
</#if>
<#if swagger>
@ApiModel(value = "${entity}-Form对象", description = "${table.comment!}")
</#if>
<#if entitySerialVersionUID>
public class ${entity}Form extends Page implements Serializable {
<#else>
public class ${entity}Form extends Page {
</#if>
<#if entitySerialVersionUID>

    private static final long serialVersionUID = 1L;
</#if>
}
