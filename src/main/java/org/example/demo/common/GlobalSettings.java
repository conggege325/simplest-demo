package org.example.demo.common;

public class GlobalSettings {

    public static final String LOGIN_USER = "user";

    public static final String LOGIN_USER_TOKEN_IN_COOKIE = "__authorization__";

    public static final String LOGIN_USER_TOKEN_IN_HEADER = "Authorization";

    public static final String LOGIN_USER_PREFIX = "user:";

    public static final String LOGIN_USER_BLACK_LIST_PREFIX = "ubl:";
}
