package org.example.demo.common;

public enum Error {

    COMMON(10000, "服务器内部错误");

    private Integer code;

    private String message;

    Error(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
