package org.example.demo.entity.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 操作员表
 * </p>
 *
 * @author CGG
 * @since 2024-01-18
 */
@Getter
@Setter
@TableName("ac_operator")
@ApiModel(value = "Operator对象", description = "操作员表")
public class Operator implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("操作员id")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty("登录帐号")
    @TableField("username")
    private String username;

    @ApiModelProperty("操作员姓名")
    @TableField("nickname")
    private String nickname;

    @ApiModelProperty("密码")
    @TableField("password")
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String password;

    @ApiModelProperty("操作员状态：1 正常；2 冻结")
    @TableField("status")
    private Integer status;

    @ApiModelProperty("所属机构id")
    @TableField("org_id")
    private Integer orgId;

    @ApiModelProperty("联系电话")
    @TableField("phone_number")
    private String phoneNumber;

    @ApiModelProperty("电子邮件")
    @TableField("email")
    private String email;

    @ApiModelProperty("创建者id")
    @TableField("creator_id")
    private Integer creatorId;

    @ApiModelProperty("创建时间")
    @TableField("create_time")
    private Date createTime;


}
