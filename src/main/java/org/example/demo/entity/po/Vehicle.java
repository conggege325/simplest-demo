package org.example.demo.entity.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 基础数据-车辆信息表
 * </p>
 *
 * @author CGG
 * @since 2024-04-23
 */
@Getter
@Setter
@TableName("bd_vehicle")
@ApiModel(value = "Vehicle对象", description = "基础数据-车辆信息表")
public class Vehicle implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("车辆代码")
    @TableId(value = "code", type = IdType.ASSIGN_ID)
    private String code;

    @ApiModelProperty("车牌号")
    @TableField("license_plate_number")
    private String licensePlateNumber;

    @ApiModelProperty("车辆状态：1-正常使用 2-调拨中 3-维修中 4-封存中 5-已报废")
    @TableField("state")
    private Integer state;

    @ApiModelProperty("使用单位")
    @TableField("use_org")
    private String useOrg;

    @ApiModelProperty("注册日期")
    @TableField("registration_date")
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    private Date registrationDate;

    @ApiModelProperty("容积")
    @TableField("volume")
    private String volume;

    @ApiModelProperty("创建时间")
    @TableField("create_time")
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    @ApiModelProperty("创建人")
    @TableField("create_user")
    private String createUser;
}
