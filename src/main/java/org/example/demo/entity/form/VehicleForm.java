package org.example.demo.entity.form;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.Setter;
import java.io.Serializable;

/**
 * <p>基础数据-车辆信息表</p>
 *
 * @author CGG
 * @since 2024-04-23
 */
@Getter
@Setter
@ApiModel(value = "Vehicle-Form对象", description = "基础数据-车辆信息表")
public class VehicleForm extends Page implements Serializable {

    private static final long serialVersionUID = 1L;
}
