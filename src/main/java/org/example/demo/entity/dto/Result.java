package org.example.demo.entity.dto;

import lombok.Data;
import lombok.experimental.Accessors;
import org.example.demo.common.Error;

import java.io.Serializable;

@Data
@Accessors(chain = true)
public class Result<T> implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer code = 0;

    private String message;

    private T data;

    public Result() {
    }

    public Result(Integer code, T data, String message) {
        this.code = code;
        this.message = message;
        this.data = data;
    }

    public static Result ok() {
        Result result = new Result();
        return result;
    }

    public static <S> Result ok(S data) {
        Result<S> result = new Result();
        result.setData(data);
        return result;
    }

    public static Result error(int code) {
        Result result = new Result();
        result.setCode(code);
        return result;
    }

    /**
     * 通用错误
     */
    public static Result error(String msg) {
        Result result = new Result();
        result.setCode(10000);
        result.setMessage(msg);
        return result;
    }

    public static Result error(int code, String msg) {
        Result result = new Result();
        result.setCode(code);
        result.setMessage(msg);
        return result;
    }

    public static Result<Boolean> build(Error error) {
        Result result = new Result();
        result.setData(true);
        result.setCode(error.getCode());
        result.setMessage(error.getMessage());
        return result;
    }
}
