package org.example.demo.entity.dto;

import lombok.Data;

import java.util.List;

@Data
public class LoginUser {

    private Integer userid;

    private String username;

    private Integer orgId;

    private List<Integer> roleIds;
}
