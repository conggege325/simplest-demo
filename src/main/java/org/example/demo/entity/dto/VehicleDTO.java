package org.example.demo.entity.dto;

import org.example.demo.entity.po.Vehicle;
import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.Setter;
import java.io.Serializable;

/**
 * <p>基础数据-车辆信息表</p>
 *
 * @author CGG
 * @since 2024-04-23
 */
@Getter
@Setter
@ApiModel(value = "Vehicle-DTO对象", description = "基础数据-车辆信息表")
public class VehicleDTO extends Vehicle implements Serializable {

    private static final long serialVersionUID = 1L;
}
