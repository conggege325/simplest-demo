package org.example.demo.utils;

import cn.hutool.core.util.StrUtil;
import org.example.demo.common.GlobalSettings;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

public class RequestUtil {

    public static String getToken(HttpServletRequest request) {
        String token = request.getHeader(GlobalSettings.LOGIN_USER_TOKEN_IN_HEADER);
        if (StrUtil.isNotBlank(token)) {
            return token;
        }
        Cookie[] cookies = request.getCookies();
        if (cookies != null) {
            for (Cookie cookie : cookies) {
                if (GlobalSettings.LOGIN_USER_TOKEN_IN_COOKIE.equals(cookie.getName())) {
                    return cookie.getValue();
                }
            }
        }
        return null;
    }
}
