package org.example.demo.utils;

import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONObject;
import cn.hutool.jwt.JWT;

public class JwtUtil {

    public static boolean checkJwtToken(String jwtSecretKey, String token) {
        if (StrUtil.isBlank(token)) {
            return false;
        }
        JWT jwt = JWT.of(token).setKey(jwtSecretKey.getBytes());
        if (!jwt.validate(0)) {
            return false;
        }
        return true;
    }

    public static JSONObject getJwtPayloads(String jwtSecretKey, String token) {
        if (StrUtil.isBlank(token)) {
            return null;
        }
        JWT jwt = JWT.of(token).setKey(jwtSecretKey.getBytes());
        if (!jwt.validate(0)) {
            return null;
        }
        return jwt.getPayloads();
    }
}
