package org.example.demo.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.example.demo.entity.dto.Result;
import org.example.demo.entity.form.OperatorSearchForm;
import org.example.demo.entity.po.Vehicle;
import org.example.demo.service.VehicleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 基础数据-车辆信息表 前端控制器
 * </p>
 *
 * @author CGG
 * @since 2024-04-23
 */
@RestController
@RequestMapping("/vehicle")
@Api(tags = "车辆信息")
public class VehicleController {

    @Autowired
    private VehicleService vehicleService;

    @GetMapping("/list")
    @ResponseBody
    @ApiOperation(value = "车辆列表查询")
    public Result<Page<Vehicle>> list(OperatorSearchForm form) {
        Page<Vehicle> operators = vehicleService.page(form);
        return Result.ok(operators);
    }
}
