package org.example.demo.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.example.demo.common.Error;
import org.example.demo.entity.dto.Result;
import org.example.demo.entity.form.OperatorSearchForm;
import org.example.demo.entity.po.Operator;
import org.example.demo.service.OperatorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.Date;

/**
 * <p>
 * 操作员表 前端控制器
 * </p>
 *
 * @author CGG
 * @since 2024-01-18
 */
@Controller
@RequestMapping("/operator")
@Api(tags = "操作员API")
@Slf4j
public class OperatorController {

    @Autowired
    private OperatorService operatorService;

    @GetMapping("")
    @ApiOperation(value = "操作员列表查询")
    public String index(OperatorSearchForm form, Model model) {
        Page<Operator> page = operatorService.page(form);
        model.addAttribute("operators", page.getRecords());
        return "operator/index";
    }

    @GetMapping("/list")
    @ResponseBody
    @ApiOperation(value = "操作员列表查询")
    public Result<Page<Operator>> list(OperatorSearchForm form) {
        Page<Operator> operators = operatorService.page(form);
        return Result.ok(operators);
    }

    @GetMapping("/id")
    @ResponseBody
    @ApiOperation(value = "操作员列表查询")
    public Result<Operator> id(@RequestParam Integer id) {
        Operator operator = operatorService.getById(id);
        return Result.ok(operator);
    }

    @PostMapping("/add")
    @ResponseBody
    @ApiOperation(value = "操作员列表查询")
    public Result<Boolean> add(@RequestBody Operator operator) {
        operator.setCreateTime(new Date());
        Error error = operatorService.add(operator);
        return Result.build(error);
    }
}
