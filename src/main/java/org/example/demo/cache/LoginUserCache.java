package org.example.demo.cache;

import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson2.JSONObject;
import com.github.benmanes.caffeine.cache.Caffeine;
import com.github.benmanes.caffeine.cache.LoadingCache;
import org.example.demo.common.GlobalSettings;
import org.example.demo.entity.dto.LoginUser;
import org.example.demo.service.OperatorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;

/**
 * 文档：https://blog.csdn.net/weixin_36380516/article/details/130676072
 */
@Component
public class LoginUserCache {

    @Autowired
    private RedisTemplate<String, Object> redisTemplate;

    @Autowired
    private OperatorService operatorService;

    private LoadingCache<String, LoginUser> loadingCache;

    private static final ThreadLocal<String> loginUserThreadLocal = ThreadLocal.withInitial(() -> null);

    public LoginUserCache() {
        loadingCache = Caffeine.newBuilder()
                .expireAfterAccess(15, TimeUnit.MINUTES)
                .build(username -> {
                    JSONObject json = (JSONObject) redisTemplate.opsForValue().get(GlobalSettings.LOGIN_USER_PREFIX + username);
                    if (json != null) {
                        return json.to(LoginUser.class);
                    }
                    LoginUser loginUser = operatorService.getLoginUserDetail(username);
                    if (loginUser != null) {
                        redisTemplate.opsForValue().set(GlobalSettings.LOGIN_USER_PREFIX + username, loginUser, 30, TimeUnit.MINUTES);
                    }
                    return loginUser;
                });
    }

    public LoginUser get(String username) {
        return loadingCache.get(username);
    }

    public LoginUser get() {
        String username = loginUserThreadLocal.get();
        if (StrUtil.isNotBlank(username)) {
            return loadingCache.get(username);
        }
        return null;
    }

    public void set(String username) {
        loginUserThreadLocal.set(username);
    }
}
