package org.example.demo.service;

import org.example.demo.entity.po.Vehicle;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 基础数据-车辆信息表 服务类
 * </p>
 *
 * @author CGG
 * @since 2024-04-23
 */
public interface VehicleService extends IService<Vehicle> {

}
