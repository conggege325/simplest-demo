package org.example.demo.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.example.demo.common.Error;
import org.example.demo.entity.dto.LoginUser;
import org.example.demo.entity.po.Operator;
import org.example.demo.mapper.OperatorMapper;
import org.example.demo.service.OperatorService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 操作员表 服务实现类
 * </p>
 *
 * @author CGG
 * @since 2024-01-18
 */
@Service
@Slf4j
public class OperatorServiceImpl extends ServiceImpl<OperatorMapper, Operator> implements OperatorService {

    @Override
    public LoginUser getLoginUserDetail(String username) {
        return baseMapper.getLoginUserDetail(username);
    }

    @Override
    public Error add(Operator operator) {
        baseMapper.insert(operator);
        return null;
    }
}
