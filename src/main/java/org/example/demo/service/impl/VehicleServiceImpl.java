package org.example.demo.service.impl;

import com.baomidou.dynamic.datasource.annotation.DS;
import org.example.demo.entity.po.Vehicle;
import org.example.demo.mapper.VehicleMapper;
import org.example.demo.service.VehicleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 基础数据-车辆信息表 服务实现类
 * </p>
 *
 * @author CGG
 * @since 2024-04-23
 */
@Service
@DS("base-data")
public class VehicleServiceImpl extends ServiceImpl<VehicleMapper, Vehicle> implements VehicleService {

}
