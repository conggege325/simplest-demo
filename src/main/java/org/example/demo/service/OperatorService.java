package org.example.demo.service;

import org.example.demo.common.Error;
import org.example.demo.entity.dto.LoginUser;
import org.example.demo.entity.po.Operator;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 操作员表 服务类
 * </p>
 *
 * @author CGG
 * @since 2024-01-18
 */
public interface OperatorService extends IService<Operator> {

    LoginUser getLoginUserDetail(String username);

    Error add(Operator operator);
}
