package org.example.demo.service;

import java.sql.Connection;
import java.util.List;

public interface DatasourceService {
    List getTables(Connection connection);

    List getColumns(Connection connection, String tableName);
}
