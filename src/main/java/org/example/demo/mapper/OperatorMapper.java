package org.example.demo.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.example.demo.entity.dto.LoginUser;
import org.example.demo.entity.po.Operator;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 操作员表 Mapper 接口
 * </p>
 *
 * @author CGG
 * @since 2024-01-18
 */
@Mapper
public interface OperatorMapper extends BaseMapper<Operator> {

    LoginUser getLoginUserDetail(@Param("username") String username);
}
