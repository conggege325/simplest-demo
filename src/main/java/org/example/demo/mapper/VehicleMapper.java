package org.example.demo.mapper;

import org.example.demo.entity.po.Vehicle;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 基础数据-车辆信息表 Mapper 接口
 * </p>
 *
 * @author CGG
 * @since 2024-04-23
 */
@Mapper
public interface VehicleMapper extends BaseMapper<Vehicle> {

}
